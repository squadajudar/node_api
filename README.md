# API do sistema "A Rede"

Criado para o Projeto de Integração UNA 2020A

> ## SQUAD AJUDAR
> `Autores:`
> - Daniel Henrique Matos de Paiva
> - Francisco Augusto Fontoura
> - Gabriel Henrique Souza Prado
> - João Pedro Coelho Jácome
> - Wesley Dias Maciel
> ---

## Desenvolvimento de Web Site e Aplicativo para Dispositivos Móveis da
OSCIP Engenheiros Sem Fronteiras - Núcleo BH

- A Organização da Sociedade Civil de Interesse Público (OSCIP) Engenheiros Sem Fronteira (ESF-BH): apoio social em Belo Horizonte e região metropolitana. 
- Desde sua fundação em 2015: atuação em mais de 50 projetos, beneficiando mais de 3.500 pessoas.
- Plataforma digital: divulgar informação, promover o voluntariado, receber doações, criar uma rede entre entidades do terceiro setor e comunidades carentes, promover interação entre prestadores de serviço e  a população carente.


# Pré requisitos

- Git (https://git-scm.com/)
- Node (https://nodejs.org)
- NPM (https://www.npmjs.com/)

# Instalação

## Start

Primeiro você precisa instalar o [Node.js](https://nodejs.org/en/) no seu pc e seguir o procedimento padrão de instalação **next->next->ok**, recomendo você baixar o [Git](https://git-scm.com/downloads) e instalar na sua maquina, depois basta copiar a URL do projeto conforme abaixo:

## Clonando o Repositório
Com o Git e o Node.js instalado na sua maquina e a **URL** do projeto em mãos, cria em algum lugar do seu pc uma pasta para criarmos uma copia do repositório, dentro dela abra o **cmd** , **powershell**, **bash**, etc, e digite os comandos abaixo:
```
git clone git@bitbucket.org:squadajudar/api.git
cd api
npm install
```

## .ENV
Aqui você deve configurar o `TimeZone` e os dados de conexão com o banco Mongo DB
```
TZ = 'America/Sao_Paulo'

MONGO_URL='http://localhost'
MONGO_DB=database
MONGO_USER=root
MONGO_PASS=
```

Recomendamos utilizar o serviço online do [Mongo DB Atlas](https://www.mongodb.com/cloud/atlas) para testes por ser gratuito e bem simples de se utiizar

## 1.., 2..., 3... Testando!
Agora dentro da pasta **api** você pode abrir o **cmd** , **powershell**, **bash**, etc, e executar o comando:
```
npm install
node src/index.js
```

![IBM](http://cdn.nerdvana.com.br/github/powershell.gif)

## Endpoints

**Listar todas as OSCs / ONGs.**

* **URL**

  `/ongs`

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200 OK<br />
    **Content:**
    ```json
    [
      {
        "address": {
          "cep": "...",
          "logradouro": "...",
          "numero": "...",
          "complemento": "..",
          "bairro": "...",
          "localidade": "...",
          "uf": "...",
          "latitude": 0,
          "longitude": 0
        },
        "_id": "...",
        "name": "...",
        "email": "...",
        "phone": "...",
        "description": "...",
        "members": [
          {
            "access": 0,
            "_id": "...",
            "user": "..."
          },
          ...
        ],
        "social": [
          {
            "_id": "...",
            "name": "...",
            "link": "..."
          },
          ...
        ]
      },
      ...
    ]
    ```

**Exibir dados de uma OSC / ONG específica.**

* **URL**

  `/ong/:id`

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200 OK<br />
    **Content:**
    ```json
    [
      {
        "address": {
          "cep": "...",
          "logradouro": "...",
          "numero": "...",
          "complemento": "..",
          "bairro": "...",
          "localidade": "...",
          "uf": "...",
          "latitude": 0,
          "longitude": 0
        },
        "_id": "...",
        "name": "...",
        "email": "...",
        "phone": "...",
        "description": "...",
        "members": [
          {
            "access": 0,
            "_id": "...",
            "user": "..."
          },
          ...
        ],
        "social": [
          {
            "_id": "...",
            "name": "...",
            "link": "..."
          },
          ...
        ]
      },
      ...
    ]
    ```
* **Error Response:**

  * **Code:** 404 Not Found <br />
    **Content:** 
    ```json
    { "message": "OSC / ONG não encontrada" }
    ```

**Listar todas as comunidades.**

* **URL**

  `/communities`

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200 OK<br />
    **Content:**
    ```json
    [
      {
        "address": {
          "cep": "...",
          "logradouro": "...",
          "numero": "...",
          "complemento": "..",
          "bairro": "...",
          "localidade": "...",
          "uf": "...",
          "latitude": 0,
          "longitude": 0
        },
        "_id": "...",
        "name": "...",
        "email": "...",
        "phone": "...",
        "description": "...",
        "members": [
          {
            "access": 0,
            "_id": "...",
            "user": "..."
          },
          ...
        ],
        "social": [
          {
            "_id": "...",
            "name": "...",
            "link": "..."
          },
          ...
        ]
      },
      ...
    ]
    ```

**Exibir dados de uma comunidade específica.**

* **URL**

  `/community/:id`

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200 OK<br />
    **Content:**
    ```json
    [
      {
        "address": {
          "cep": "...",
          "logradouro": "...",
          "numero": "...",
          "complemento": "..",
          "bairro": "...",
          "localidade": "...",
          "uf": "...",
          "latitude": 0,
          "longitude": 0
        },
        "_id": "...",
        "name": "...",
        "email": "...",
        "phone": "...",
        "description": "...",
        "members": [
          {
            "access": 0,
            "_id": "...",
            "user": "..."
          },
          ...
        ],
        "social": [
          {
            "_id": "...",
            "name": "...",
            "link": "..."
          },
          ...
        ]
      },
      ...
    ]
    ```
* **Error Response:**

  * **Code:** 404 Not Found <br />
    **Content:** 
    ```json
    { "message": "Comunidade não encontrada" }
    ```

**Efetuar Cadastro.**

* **URL**

  `/user/new`

* **Method:**

  `POST`

* **Data Params**

  **Required**
  ```json
  {
    "name": "...",
    "email": "...",
    "password": "..."
  }
  ```

* **Success Response:**

  * **Code:** 200 OK<br />
    **Content:**
    ```json
    { "message": "Usuário cadastrado com sucesso" }
    ```
 
* **Error Response:**

  * **Code:** 401 Bad Request<br />
    **Content:** 
    ```json
    { "message": "E-mail já cadastrado!" }
    ```

**Efetuar Login.**

* **URL**

  `/user/login`

* **Method:**

  `POST`

* **Data Params**

  **Required**
  ```json
  {
    "email": "...",
    "password": "..."
  }
  ```

* **Success Response:**

  * **Code:** 200 OK<br />
    **Content:**
    ```json
    "name": "...",
    "about": "...",
    "email": "...",
    "showContact": false,
    "admin": false,
    "token": "...,
    ```
 
* **Error Response:**

  * **Code:** 403 Forbidden<br />
    **Content:** 
    ```json
    { "message": "Login ou senha incorretos" }
    ```
