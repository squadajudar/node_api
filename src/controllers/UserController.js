const User = require('../models/User');
const cryptoJs = require('crypto-js');

function normatizeLower(value) {
  if (value) {
    return value.trim().toLowerCase();
  } else {
    return '';
  }
}

module.exports = {
  async index(req, res) {
    const users = await User.find();

    return res.status(200).json(users)
  },

  async login(req, res) {
    const dta = new Date();
    let { email, password } = req.body;
    email = normatizeLower(email);
    password = cryptoJs.SHA256(password).toString();

    const token = cryptoJs.SHA256(dta + password).toString();

    let user = await User.findOne({
      email,
      password
    })

    if (user) {
      user.token = token;
      await User.updateOne({ email }, {
        $set: { token }
      });

      return res.status(200).json(user)
    } else {
      return res.status(403).json({ message: "Login ou senha incorretos" })
    }
  },

  async store(req, res) {
    const { name, email, password } = req.body;
    let user = await User.findOne({ email })
    if (!user) {
      user = await User.create({
        name,
        email: normatizeLower(email),
        password: cryptoJs.SHA256(password).toString(),
      });

      return res.json({ message: "Usuário cadastrado com sucesso" });
    } else {
      return res.status(401).json({ message: "E-mail já cadastrado!" })
    }
  },

  async updateBio(req, res) {
    const { newBio } = req.body;

    const user = await User.findOne({ _id: req.user._id });

    user.about = newBio;

    user.save();

    return res.status(200).json({ message: 'Biografia atualizada com sucesso!', user })
  },

  async updatePass(req, res) {
    const { oldPass, newPass } = req.body;

    const user = await User.findOne({ _id: req.user.id }).select('password');

    if (user.password !== cryptoJs.SHA256(oldPass).toString()) {
      return res.status(403).json({ message: 'Senha incorreta' })
    }

    user.password = cryptoJs.SHA256(newPass).toString();

    user.save();

    return res.status(200).json({ message: 'Senha atualizada com sucesso!' });
  },

  async updatePrivacy(req, res) {
    const { privacy } = req.body;

    const user = await User.findOne({ _id: req.user.id });

    user.showContact = privacy;

    user.save();

    return res.status(200).json({ message: 'Privacidade atualizada com sucesso!', user });
  },

  async destroy(req, res) {
    await User.findByIdAndDelete({ _id: req.params.id })

    return res.json({ message: "Usuário removido com sucesso" });
  }

};
