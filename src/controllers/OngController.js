const Ong = require('../models/Ong');
const User = require('../models/User');
const mongoose = require('mongoose');

module.exports = {
  async index(req, res) {
    const ongs = await Ong.find();

    return res.status(200).json(ongs);
  },

  async list(req, res) {
    const ongs = await Ong.find();

    const oscs = ongs.filter(ong => ong.members.some(member => member.user.equals(req.user._id)));

    return res.status(200).json(oscs);
  },

  async show(req, res) {
    let ong;
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      ong = await Ong.findOne({ _id: req.params.id }).populate('members.user', '-token');
    }

    if (!ong) {
      return res.status(404).json({ message: 'OSC / ONG não encontrada' })
    }

    return res.status(200).json(ong);
  },

  async store(req, res) {
    let ong = await Ong.findOne({ email: req.body.email });
    const user = req.user._id

    if (ong) {
      return res.status(400).json({ message: "OSC / ONG já cadastrada", ong })
    }

    ong = await Ong.create(req.body);

    ong.members.push({ user, access: 10 });

    await ong.save();

    return res.status(200).json({ message: "OSC / ONG cadastrada com sucesso", ong });
  },

  async setUser(req, res) {

    const ong = await Ong.findById(req.params.id).populate('members.user', '-token');

    const user = await User.findById(req.body.user);

    if (!user) {
      return res.status(404).json({ message: "Usuário não encontrado" })
    }

    ong.members.push({ user, access: 0 });

    await ong.save();

    return res.status(200).json({ message: 'Bem Vindo!', ong });
  },

  async setSocial(req, res) {

    const ong = await Ong.findById(req.params.id);

    const ongUser = ong.members.find(member => member.user.equals(req.user._id));

    const { name, link } = req.body;

    if (!ongUser) {
      return res.status(403).json({ message: 'Usuáro não autorizado' });
    }

    if (ongUser.access < 9) {
      return res.status(403).json({ message: 'Usuáro não autorizado' })
    }

    if (!name || !link) {
      return res.status(403).json({ message: 'Dados Incorretos' });
    }

    ong.social.push({ name, link });

    await ong.save();

    return res.status(200).json({ message: 'Rede Social adicionada com sucesso', ong });
  },

  async authorityTransfer(req, res) {

    const ong = await Ong.findById(req.params.id).populate('members.user', '-token');
    const ongUser = await ong.members.find(member => member.user.equals(req.user._id));

    const user = await ong.members.find(member => member.equals(req.body.id));

    if (ongUser && ongUser.access > 0 && user && ongUser.access > user.access) {
      user.access = ongUser.access;
      ongUser.access = 0;
      await ong.save();

      return res.status(200).json({ message: 'Permissões transferidas com sucesso', ong })
    } else {
      return res.status(403).json({ message: 'Não autorizado' })
    }
  },

  async updateAddress(req, res) {
    const address = req.body;
    const ong = await Ong.findById(req.params.id).populate('members.user', '-token');
    const ongUser = await ong.members.find(member => member.user.equals(req.user._id));

    if (ongUser && ongUser.access >= 9) {
      ong.address = address
      await ong.save();

      return res.status(200).json({ message: 'Endereço atualizado com sucesso!', ong })
    } else {
      return res.status(403).json({ message: 'Não autorizado' })
    }
  },

  async updateUser(req, res) {
    const ong = await Ong.findById(req.params.id).populate('members.user', '-token');

    const ongUser = ong.members.find(member => member.user.equals(req.user._id));
    const ongMember = ong.members.find(member => member.equals(req.body.id));

    if (!ongUser) {
      return res.status(403).json({ message: 'Usuáro não autorizado' })
    }

    if (ongUser.access < 9) {
      return res.status(403).json({ message: 'Usuáro não autorizado' })
    }

    ongMember.access = req.body.access;

    await ong.save();

    return res.status(200).json({ message: 'Membro adicionado com sucesso', ong })
  }
}
