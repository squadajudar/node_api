const Community = require('../models/Community');
const User = require('../models/User');
const mongoose = require('mongoose');

module.exports = {
  async index(req, res) {
    const communities = await Community.find();

    return res.status(200).json(communities);
  },

  async list(req, res) {
    const communities = await Community.find();

    const mycommunities = communities.filter(community => community.members.some(member => member.user.equals(req.user._id)));

    return res.status(200).json(mycommunities);
  },

  async show(req, res) {
    let community;

    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      community = await Community.findById(req.params.id).populate('members.user', '-token');
    }

    if (!community) {
      return res.status(404).json({ message: 'Comunidade não encontrada' })
    }

    return res.status(200).json(community);
  },

  async store(req, res) {
    let community = await Community.findOne({ email: req.body.email });
    const user = req.user._id

    if (community) {
      return res.status(400).json({ message: "Comunidade já cadastrada", community })
    }

    community = await Community.create(req.body);

    community.members.push({ user, access: 10 });

    await community.save();

    return res.status(200).json({ message: "Comunidade cadastrada com sucesso", community });
  },

  async setUser(req, res) {

    const community = await Community.findById(req.params.id).populate('members.user', '-token');

    const user = await User.findById(req.body.user);

    if (!user) {
      return res.status(404).json({ message: 'Usuário não encontrado' })
    }

    community.members.push({ user, access: 0 });

    await community.save();

    return res.status(200).json({ message: 'Bem Vindo!', community });
  },

  async setSocial(req, res) {

    const community = await Community.findById(req.params.id).populate('members.user', '-token');

    const communityUser = community.members.find(member => member.user.equals(req.user._id));

    const { name, link } = req.body;

    if (!communityUser) {
      return res.status(403).json({ message: 'Usuáro não autorizado' });
    }

    if (communityUser.access < 9) {
      return res.status(403).json({ message: 'Usuáro não autorizado' })
    }

    if (!name || !link) {
      return res.status(403).json({ message: 'Dados Incorretos' });
    }

    community.social.push({ name, link });

    await community.save();

    return res.status(200).json({ message: 'Rede Social adicionada com sucesso', community });
  },

  async authorityTransfer(req, res) {

    const community = await Community.findById(req.params.id).populate('members.user', '-token');
    const communityUser = await community.members.find(member => member.user.equals(req.user._id));

    const user = await community.members.find(member => member.user.equals(req.body.user.id));

    if (communityUser && communityUser.access > 0 && user && communityUser.access > user.access) {
      user.access = communityUser.access;
      communityUser.access = 0;
      await community.save();

      return res.status(200).json({ message: 'Permissões transferidas com sucesso' })
    } else {
      return res.status(403).json({ message: 'Não autorizado' })
    }
  },

  async updateAddress(req, res) {
    const address = req.body;
    const community = await Community.findById(req.params.id).populate('members.user', '-token');
    const communityUser = await community.members.find(member => member.user.equals(req.user._id));

    if (communityUser && communityUser.access >= 9) {
      community.address = address
      await community.save();

      return res.status(200).json({ message: 'Endereço atualizado com sucesso!', community })
    } else {
      return res.status(403).json({ message: 'Não autorizado' })
    }
  },

  async updateUser(req, res) {
    const community = await Community.findById(req.params.id).populate('members.user', '-token');

    const communityUser = community.members.find(member => member.user.equals(req.user._id));
    const communityMember = community.members.find(member => member.equals(req.body.id));

    if (!communityUser) {
      return res.status(403).json({ message: 'Usuáro não autorizado' })
    }

    if (communityUser.access < 9) {
      return res.status(403).json({ message: 'Usuáro não autorizado' })
    }

    communityMember.access = req.body.access;

    await community.save();

    return res.status(200).json({ message: 'Membro adicionado com sucesso', community })
  }
}
