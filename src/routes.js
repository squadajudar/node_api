const { Router } = require('express');

const routes = Router();

const UserController = require('./controllers/UserController');
const OngController = require('./controllers/OngController');
const CommunityController = require('./controllers/CommunityController');

const auth = require('./middlewares/auth');

routes.get('/', (req, res) => {
  res.redirect('http://gabrielhenriq.com.br/efs/');
});


routes.post('/user/login', UserController.login);
routes.post('/user/new', UserController.store);

routes.get('/ongs', OngController.index);
routes.get('/ong/:id', OngController.show);

routes.get('/communities', CommunityController.index);
routes.get('/community/:id', CommunityController.show);

routes.get('/myongs', auth.check, OngController.list);
routes.get('/mycommunities', auth.check, CommunityController.list);


routes.post('/ongs/new', auth.check, OngController.store);
routes.post('/ong/:id/members/add', auth.check, OngController.updateUser);
routes.post('/ong/:id/members/apply', OngController.setUser);
routes.post('/ong/:id/social/add', auth.check, OngController.setSocial);
routes.post('/ong/:id/authorityTransfer', auth.check, OngController.authorityTransfer);
routes.post('/ong/:id/updateAddress', auth.check, OngController.updateAddress);

routes.post('/communities/new', auth.check, CommunityController.store);
routes.post('/community/:id/members/add', auth.check, CommunityController.updateUser);
routes.post('/community/:id/members/apply', CommunityController.setUser);
routes.post('/community/:id/social/add', auth.check, CommunityController.setSocial);
routes.post('/community/:id/authorityTransfer', auth.check, CommunityController.authorityTransfer);
routes.post('/community/:id/updateAddress', auth.check, CommunityController.updateAddress);

routes.post('/user/updateBio', auth.check, UserController.updateBio);
routes.post('/user/updatePass', auth.check, UserController.updatePass);
routes.post('/user/updatePrivacy', auth.check, UserController.updatePrivacy);

module.exports = routes;
