const { Schema, model } = require('mongoose');

const CommunitySchema = new Schema({
  name: {
    type: String,
    required: [true, 'Informe um nome'],
  },
  email: {
    type: String,
    require: [true, 'Informe um email'],
  },
  phone: {
    type: String,
    select: false,
  },
  description: {
    type: String,
    required: [true, 'Informe uma descrição'],
  },
  address: {
    cep: String,
    logradouro: String,
    complemento: String,
    numero: String,
    bairro: String,
    localidade: String,
    uf: String,
    latitude: Number,
    longitude: Number
  },
  members: [
    {
      user: { type: Schema.Types.ObjectId, ref: 'User' },
      access: {
        type: Number,
        default: 0
      }
    }
  ],
  social: [
    {
      name: {
        type: String,
        required: [true, 'Escolha uma rede social']
      },
      link: {
        type: String,
        required: [true, 'Informe o link da rede social']
      }
    }
  ]
}, {
  timestamps: true
});

module.exports = model('Community', CommunitySchema);
