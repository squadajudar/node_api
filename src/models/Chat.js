const { Schema, model } = require('mongoose');

const ChatSchema = new Schema({
  user: [
    {
      type: {
        type: Schema.Types.ObjectId, ref: 'User'
      }
    }
  ],
  messages: [
    {
      user: {
        type: Schema.Types.ObjectId, ref: 'User'
      },
      message: String,
      received: Boolean,
      read: Boolean,
      createdAt: Date
    },
  ]
}, {
  timestamps: true
});

module.exports = model('Chat', ChatSchema);
