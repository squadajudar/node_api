const { Schema, model } = require('mongoose');

const UserSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Informe um nome'],
  },
  about: {
    type: String,
    default: ''
  },
  email: {
    type: String,
    require: [true, 'Informe um email'],
  },
  showContact: {
    type: Boolean,
    default: false
  },
  password: {
    type: String,
    required: [true, 'Informe uma senha'],
    select: false,
  },
  admin: {
    type: Boolean,
    default: false,
  },
  token: {
    type: String,
    default: ""
  },
}, {
  timestamps: true
});

module.exports = model('User', UserSchema);
