const User = require('../models/User');

module.exports = {
  async check(req, res, next) {
    if (!req.headers.authorization) {
      return res.status(401).json({ message: "Realize o login" })
    }
    const { authorization: token } = req.headers
    const user = await User.findOne(
      {
        $and:
          [
            { "token": token },
            { "token": { $ne: null } },
            { "token": { $exists: true } },
            { "token": { "$nin": [null, ""] } }
          ]
      }
    )

    if (!user) {
      return res.status(401).json({ message: "Usuário não autorizado" })
    }

    req.user = user;

    next();
  }
}
