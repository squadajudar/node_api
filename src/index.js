require('dotenv').config();
const express = require('express');
const cors = require('cors');
const http = require('http');
const mongoose = require('mongoose');
const routes = require('./routes');

mongoose.connect(process.env.MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});

const app = express();
const server = http.Server(app);

app.use(cors());
app.use(express.json({ limit: '50mb', extended: true }));
app.use(routes);

server.listen(process.env.PORT || 3333);
